use directories::UserDirs;
use std::{
    collections::HashMap,
    path::{
        Path,
        PathBuf,
    },
    str::FromStr,
};

pub fn try_nuke_all_settings() {
    let settings_dir = match Settings::settings_dir() {
        Some(d) => d,
        None => return,
    };

    let iter = match std::fs::read_dir(settings_dir) {
        Ok(i) => i,
        Err(_e) => return,
    };

    for entry in iter {
        match entry {
            Ok(entry) => {
                let mut path = entry.path();
                path.push("GameSettings.ini");
                match Settings::load_from_path(&path) {
                    Ok(mut settings) => {
                        nuke_settings(&mut settings);
                        match std::fs::File::create(path) {
                            Ok(mut file) => {
                                let _ = serde_ini::to_writer(&mut file, &settings);
                            }
                            Err(_e) => {}
                        };
                    }
                    Err(_e) => {}
                }
            }
            Err(_e) => {}
        }
    }
}

pub fn nuke_settings(settings: &mut Settings) {
    settings.display_settings.resolution_width = 800;
    settings.display_settings.resolution_height = 600;
    settings.display_settings.use_letterbox = 1;
    settings.display_settings.window_mode = 1;

    settings.input.invert_axis_y = 1;
    settings.input.invert_mouse_axis_y = 1;
    settings.input.toggle_aim = 1;
    settings.input.toggle_sprint = 1;
    settings.input.toggle_walk = 1;

    settings.display.brightness = 100.0;
    settings.display.fps_limit = 30;

    settings.online.data_center_hint = String::from("japanwest");
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Settings {
    #[serde(rename = "DISPLAY_SETTINGS")]
    pub display_settings: DisplaySettings,

    #[serde(rename = "HARDWARE_INFO")]
    pub hardware_info: HardwareInfo,

    #[serde(rename = "QUALITY_SETTINGS")]
    pub quality_settings: QualitySettings,

    #[serde(rename = "CUSTOM_PRESET_SETTINGS")]
    pub custom_preset_settings: CustomPresetSettings,

    #[serde(rename = "READ_ONLY")]
    pub read_only: ReadOnly,

    #[serde(rename = "INPUT")]
    pub input: Input,

    #[serde(rename = "DISPLAY")]
    pub display: Display,

    #[serde(rename = "AUDIO")]
    pub audio: Audio,

    #[serde(rename = "GAMEPLAY")]
    pub gameplay: Gameplay,

    #[serde(rename = "ONLINE")]
    pub online: Online,

    #[serde(flatten)]
    pub extra: HashMap<String, HashMap<String, String>>,
}

#[derive(Debug)]
pub enum LoadfromPathError {
    Io(std::io::Error),
    Ini(serde_ini::de::Error),
}

impl Settings {
    pub fn settings_dir() -> Option<PathBuf> {
        UserDirs::new().and_then(|d| Some(d.document_dir()?.join("My games\\Rainbow Six - Siege")))
    }

    pub fn load_from_path(p: &Path) -> Result<Self, LoadfromPathError> {
        let file = std::fs::File::open(p).map_err(LoadfromPathError::Io)?;
        serde_ini::from_read(file).map_err(LoadfromPathError::Ini)
    }

    pub fn load_all_local() -> std::io::Result<Vec<Result<Self, LoadfromPathError>>> {
        let settings_dir = match Self::settings_dir() {
            Some(d) => d,
            None => return Ok(Vec::new()),
        };

        std::fs::read_dir(settings_dir)?
            .map(|entry| {
                entry
                    .map(|entry| {
                        let mut path = entry.path();
                        path.push("GameSettings.ini");
                        path
                    })
                    .map(|path| Self::load_from_path(&path))
            })
            .collect()
    }

    pub fn understands_all_keys(&self) -> bool {
        self.display_settings.extra.is_empty()
            && self.hardware_info.extra.is_empty()
            && self.quality_settings.extra.is_empty()
            && self.custom_preset_settings.extra.is_empty()
            && self.read_only.extra.is_empty()
            && self.input.extra.is_empty()
            && self.display.extra.is_empty()
            && self.audio.extra.is_empty()
            && self.gameplay.extra.is_empty()
            && self.online.extra.is_empty()
            && self.extra.is_empty()
    }
}

impl FromStr for Settings {
    type Err = serde_ini::de::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_ini::from_str(s)
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct DisplaySettings {
    #[serde(rename = "GPUAdapter")]
    pub gpu_adapter: String,

    #[serde(rename = "Monitor")]
    pub monitor: String,

    /// 0 for autodetection
    #[serde(rename = "ResolutionWidth")]
    pub resolution_width: u32,

    /// 0 for autodetection
    #[serde(rename = "ResolutionHeight")]
    pub resolution_height: u32,

    #[serde(rename = "RefreshRate")]
    pub refresh_rate: f32,

    /// 0 fullscreen / 1 windowed / 2 borderless
    #[serde(rename = "WindowMode")]
    pub window_mode: u8,

    /// 0 display / 1 resolution / 2..N as options
    #[serde(rename = "AspectRatio")]
    pub aspect_ratio: u8,

    /// 0 disabled / 1 frame / 2 frames
    #[serde(rename = "VSync")]
    pub vsync: u8,

    #[serde(rename = "MaxGPUBufferedFrame")]
    pub max_gpu_buffered_frame: String,

    /// 0 disabled / 1 enabled
    #[serde(rename = "UseLetterbox")]
    pub use_letterbox: u8,

    /// Default vertical field of view (degrees)
    #[serde(rename = "DefaultFOV")]
    pub default_fov: f32,

    #[serde(rename = "EnableAMDMultiDraw")]
    pub enable_amd_multidraw: String,

    /// Sharpen filter applied to TAA
    #[serde(rename = "TAASharpenFactor")]
    pub taa_sharpen_factor: f32,

    /// Percentage of the display resolution at which 3D rendering is performed
    #[serde(rename = "RenderScaling")]
    pub render_scaling: u8,

    /// Percentage of the display resolution at which TAA is performed
    #[serde(rename = "TAAScaling")]
    pub taa_scaling: String,

    /// Enables usage of AMD AGS library for additional info in crash reports
    #[serde(rename = "UseAmdAGS")]
    pub use_amd_ags: String,

    /// Enable dynamic resolution on vulkan
    #[serde(
        rename = "UseDynamicResolution",
        skip_serializing_if = "Option::is_none"
    )]
    pub use_dynamic_resolution: Option<u32>,

    /// Target FPS on the GPU when dynamic resolution is enabled
    #[serde(
        rename = "DynamicResolutionTargetFPS",
        skip_serializing_if = "Option::is_none"
    )]
    pub dynamic_resolution_target_fps: Option<u32>,

    /// Minimum render scaling value the dynamic resolution will be able to reach
    #[serde(
        rename = "DynamicResolutionMin",
        skip_serializing_if = "Option::is_none"
    )]
    pub dynamic_resolution_min: Option<u32>,

    /// Maximum render scaling value the dynamic resolution will be able to reach
    #[serde(
        rename = "DynamicResolutionMax",
        skip_serializing_if = "Option::is_none"
    )]
    pub dynamic_resolution_max: Option<u32>,

    /// Semicolon separated list, with all the Vulkan layers you want to enable, like VK_LAYER_VALVE_steam_fossilize;VK_LAYER_RTSS
    #[serde(
        rename = "VulkanWhitelistedLayers",
        skip_serializing_if = "Option::is_none"
    )]
    pub vulkan_whitelisted_layers: Option<String>,

    /// -1 for centering
    #[serde(rename = "InitialWindowPositionX")]
    pub initial_window_position_x: i64,

    /// -1 for centering
    #[serde(rename = "InitialWindowPositionY")]
    pub initial_window_position_y: i64,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct HardwareInfo {
    #[serde(rename = "GPUVendor")]
    pub gpu_vendor: String,

    #[serde(rename = "GPUDeviceId")]
    pub gpu_device_id: String,

    #[serde(rename = "GPUSubSysId")]
    pub gpu_sub_sys_id: String,

    #[serde(rename = "GPUDedicatedMemoryMB")]
    pub gpu_dedicated_memory_mb: u32,

    #[serde(rename = "GPUScore")]
    pub gpu_score: f32,

    #[serde(rename = "GPUScoreConf")]
    pub gpu_score_conf: f32,

    #[serde(rename = "SystemMemoryMB")]
    pub system_memory_mb: u32,

    #[serde(rename = "CPUScore")]
    pub cpu_score: f32,

    #[serde(rename = "HardwareNotificationEnable")]
    pub hardware_notification_enable: String,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct QualitySettings {
    #[serde(rename = "QualityPresetName")]
    pub quality_preset_name: String,

    #[serde(rename = "TexturePresetName")]
    pub texture_preset_name: String,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct CustomPresetSettings {
    #[serde(rename = "Shadow")]
    pub shadow: u32,

    #[serde(rename = "Reflection")]
    pub reflection: u32,

    #[serde(rename = "LOD")]
    pub lod: u32,

    #[serde(rename = "AO")]
    pub ao: u32,

    #[serde(rename = "Shading")]
    pub shading: u32,

    #[serde(rename = "MotionBlur")]
    pub motion_blur: u32,

    #[serde(rename = "LensEffects")]
    pub lens_effects: u32,

    #[serde(rename = "DepthOfField")]
    pub depth_of_field: u32,

    #[serde(rename = "TextureFiltering")]
    pub texture_filtering: u32,

    #[serde(rename = "AntiAliasingMethod")]
    pub anti_aliasing_method: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct ReadOnly {
    #[serde(rename = "DefaultValuesVersion")]
    pub default_values_version: String,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Input {
    #[serde(rename = "RawInputMouseKeyboard")]
    pub raw_input_mouse_keyboard: u32,

    #[serde(rename = "InvertAxisY")]
    pub invert_axis_y: u32,

    #[serde(rename = "InvertMouseAxisY")]
    pub invert_mouse_axis_y: u32,

    #[serde(rename = "Rumble")]
    pub rumble: u32,

    #[serde(rename = "AimAssist")]
    pub aim_assist: u32,

    #[serde(rename = "YawSensitivity")]
    pub yaw_sensitivity: u32,

    #[serde(rename = "PitchSensitivity")]
    pub pitch_sensitivity: u32,

    #[serde(rename = "DeadzoneLeftStick")]
    pub deadzone_left_stick: u32,

    #[serde(rename = "DeadzoneRightStick")]
    pub deadzone_right_stick: u32,

    #[serde(rename = "MouseSensitivity")]
    pub mouse_sensitivity: u32,

    #[serde(rename = "MouseYawSensitivity")]
    pub mouse_yaw_sensitivity: u32,

    #[serde(rename = "MousePitchSensitivity")]
    pub mouse_pitch_sensitivity: u32,

    #[serde(rename = "MouseSensitivityMultiplierUnit")]
    pub mouse_sensitivity_multiplier_unit: f32,

    #[serde(rename = "MouseScroll")]
    pub mouse_scroll: u32,

    #[serde(rename = "XFactorAiming")]
    pub x_factor_aiming: f32,

    #[serde(rename = "ToggleAim")]
    pub toggle_aim: u32,

    #[serde(rename = "ToggleLean")]
    pub toggle_lean: u32,

    #[serde(rename = "ToggleSprint")]
    pub toggle_sprint: u32,

    #[serde(rename = "ToggleCrouch")]
    pub toggle_crouch: u32,

    #[serde(rename = "ToggleProne")]
    pub toggle_prone: u32,

    #[serde(rename = "ToggleWalk")]
    pub toggle_walk: u32,

    #[serde(rename = "ToggleGadgetDeploymentGamepad")]
    pub toggle_gadget_deployment_gamepad: u32,

    #[serde(rename = "ToggleGadgetDeploymentKeyboard")]
    pub toggle_gadget_deployment_keyboard: u32,

    #[serde(rename = "AimDownSights")]
    pub aim_down_sights: u32,

    #[serde(rename = "AimDownSightsMouse")]
    pub aim_down_sights_mouse: u32,

    #[serde(rename = "ControlSchemeIndex")]
    pub control_scheme_index: u32,

    #[serde(rename = "ControllerInputDevice")]
    pub controller_input_device: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Display {
    #[serde(rename = "Brightness")]
    pub brightness: f32,

    /// Limit the game's fps. Minimum of 30fps. Anything below will disable the fps limit.
    #[serde(rename = "FPSLimit")]
    pub fps_limit: u32,

    /// Enables the in-game console which is mapped to `/~. Will only work if the key is not already mapped.
    #[serde(rename = "Console")]
    pub console: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Audio {
    #[serde(rename = "Subtitle")]
    pub subtitle: u32,

    #[serde(rename = "Volume")]
    pub volume: f32,

    #[serde(rename = "VolumeMusic")]
    pub volume_music: f32,

    #[serde(rename = "VolumeSoundEffects")]
    pub volume_sound_effects: f32,

    #[serde(rename = "VolumeVoice")]
    pub volume_voice: f32,

    /// (0) Hi-Fi, (1) TV, (2) Night Mode
    #[serde(rename = "DynamicRangeMode")]
    pub dynamic_range_mode: u32,

    #[serde(rename = "VoiceChatPlaybackLevel")]
    pub voice_chat_playback_level: u32,

    #[serde(rename = "VoiceChatCaptureThresholdV2")]
    pub voice_chat_capture_threshold_v2: u32,

    /// 0 Auto / 1 Manual / 2 Push to talk
    #[serde(rename = "VoiceChatCaptureMode")]
    pub voice_chat_capture_mode: u32,

    #[serde(rename = "VoiceChatCaptureLevel")]
    pub voice_chat_capture_level: u32,

    #[serde(rename = "Mute")]
    pub mute: u32,

    #[serde(rename = "VoiceChatMuteAll")]
    pub voice_chat_mute_all: u32,

    #[serde(rename = "VoiceChatTeamOnly")]
    pub voice_chat_team_only: u32,

    #[serde(rename = "VoiceChatVolume")]
    pub voice_chat_volume: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Gameplay {
    #[serde(rename = "PartyPrivacyEnable")]
    pub party_privacy_enable: u32,

    #[serde(rename = "GameplayPingEnable")]
    pub gameplay_ping_enable: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
pub struct Online {
    /// default (ping based), eastus, centralus, southcentralus, westus, brazilsouth, northeurope, westeurope, southafricanorth, eastasia, southeastasia, australiaeast, australiasoutheast, japanwest
    #[serde(rename = "DataCenterHint")]
    pub data_center_hint: String,

    /// if UseProxyAutoDiscovery is set to true, http_proxy or https_proxy or all_proxy and no_proxy are going to be looked up and used for all http calls
    #[serde(rename = "UseProxyAutoDiscovery")]
    pub use_proxy_auto_discovery: u32,

    #[serde(rename = "UseUPnP")]
    pub use_upnp: u32,

    /// Extra Keys that were not understood
    #[serde(flatten)]
    pub extra: HashMap<String, String>,
}

#[cfg(test)]
mod tests {
    use super::*;

    const SETTINGS_FILE_1: &str = include_str!("GameSettings1.ini");
    const SETTINGS_FILE_2: &str = include_str!("GameSettings2.ini");
    const SETTINGS_FILE_3: &str = include_str!("GameSettings3.ini");
    const SETTINGS_FILE_4: &str = include_str!("GameSettings4.ini");
    const SETTINGS_FILE_5: &str = include_str!("GameSettings5.ini");

    const SETTINGS_FILES: &[&str] = &[
        SETTINGS_FILE_1,
        SETTINGS_FILE_2,
        SETTINGS_FILE_3,
        SETTINGS_FILE_4,
        SETTINGS_FILE_5,
    ];

    #[test]
    fn parse() {
        for (i, file) in SETTINGS_FILES.iter().enumerate() {
            let file = match Settings::from_str(file) {
                Ok(f) => f,
                Err(e) => {
                    panic!("Error parsing file {}, got error: {:#?}", i + 1, e);
                }
            };

            assert!(file.understands_all_keys());

            let reserialized = serde_ini::to_string(&file).unwrap();
            let dereserialized = Settings::from_str(&reserialized).unwrap();
            assert_eq!(file, dereserialized);
        }
    }

    #[test]
    #[ignore]
    fn load() {
        let _files = Settings::load_all_local().unwrap();
    }
}
